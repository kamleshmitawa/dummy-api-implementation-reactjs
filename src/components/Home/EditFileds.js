import React, { Component } from 'react';


class EditFileds extends Component {
    render() {
        const { data, editName, editAge,  editSalary , onEditDetails, updateEmployees, onEditCancel } = this.props
        return (
            <>
                <input name="editName" value={editName} placeholder={data.employee_name} onChange={onEditDetails} />
                <input name="editAge" value={editAge} placeholder={data.employee_age} onChange={onEditDetails} />
                <input name="editSalary" value={editSalary} placeholder={data.employee_salary} onChange={onEditDetails} />
                <button className="edit-button" title="Cancel" name="cancel" onClick={onEditCancel}>Cancel</button>
                <button className="edit-button" title="Edit" name="edit" onClick={(e) => updateEmployees(e, data)}>Done</button>

            </>
        )
    }
}

export default EditFileds