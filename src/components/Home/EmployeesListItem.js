
import React, { Component } from 'react';

class EmployeesListItem extends Component {
    render() {
        const { data, deleteEmployees, editHandler } = this.props

        return (
            <div className="list-block-item">
                    <div>
                        {data.employee_name}
                    </div>
                    <div>
                        {data.employee_age}
                    </div>
                    <div>
                        {data.employee_salary}
                    </div>
                    <div></div>
                    <div></div>
                    <div>
                    <button className="edit-button" title="Edit" name="edit" onClick={(e) => editHandler(e, data)}>Edit</button>
                    <button className="edit-button" title="Delete" name="delete" onClick={(e) => deleteEmployees(e, data.id)}>Delete </button>
                    </div>
            </div>
        )
    }
}

export default EmployeesListItem