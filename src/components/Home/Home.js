import React, { Component } from 'react';
import axios from 'axios';
import EditFileds from './EditFileds';
import EmployeesListItem from './EmployeesListItem';
import { Loader } from '../Login/Loader';
import { apiRequest } from '../../api/api';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            employeesList: [],
            isEdit: false,
            editName: '',
            editAge: '',
            editSalary: '',
            editingData: {},
            errmsg: ''
        }
    }

    componentDidMount() {
        const { employeesList } = this.state
        apiRequest('/employees', 'get')
            .then(res => {
                this.setState({ employeesList: [...employeesList, res.data], loading: false })
            })
            .catch(err => {
                this.setState({ errmsg : err.message, loading: false })
            })
    }

    onLogout = () => {
        localStorage.clear();
        this.props.history.push('/login')
    }

    updateEmployees = (e, data) => {
        const { editName, editAge, editSalary, employeesList } = this.state

        this.setState({ isEdit: true })
        const employee = {
            name: editName,
            age: editAge,
            salary: editSalary,
        }
        apiRequest(`/update/${data.id}`, 'putt', employee)
            .then(res => {
                if (res.data.name && res.data.age) {
                    let editedValue = [...employeesList];
                    editedValue && editedValue.forEach((employeesData, index) => (
                        employeesData && employeesData.forEach(value => {
                            if (value.id === data.id) {
                                value.employee_name = res.data.name;
                                value.employee_age = res.data.age;
                                value.employee_salary = res.data.salary
                            }
                        })))
                    this.setState({ employeesList: [...editedValue] })
                }
                else {
                    alert(' Duplicate entry for employee_name_unique')

                }
            })
            .catch(err => {
                this.setState({ errmsg : err.message, loading: false })
            })
        e.preventDefault();
    }

    deleteEmployees = (e, id) => {
        const { employeesList = [] } = this.state
        this.setState({ loading: true })
        apiRequest(`/delete/${id}`, 'delete')
            .then(res => {
                employeesList.map((employeesData) => {
                    employeesData.splice(employeesData.findIndex(function (i) {
                        return i.id === id;
                    }), 1);
                    return null;
                })
                this.setState({ employeesList, loading: false })
                return res
            })
            .then(resData => alert(resData.data.success.text))
            .catch(err => {
                console.log(err, 'error')
                this.setState({ errmsg : err.message, loading: false })
            })
        e.preventDefault();
    }


    onEditDetails = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }


    editHandler = (e, data) => {
        this.setState({ isEdit: true, editingData: data })

    }
    onEditCancel = () => {
        this.setState({ isEdit: false })
    }

    renderEmployees = () => {
        const { employeesList = [], editName, isEdit, editAge, editSalary, editingData } = this.state
        return (
            employeesList && employeesList.length > 0 ?
                employeesList.map(employeesData => (
                    employeesData && employeesData.length > 0 && employeesData.map(data => (
                        <div key={data.id} className="list-items">
                            {data.id === editingData.id && isEdit ?
                                <div key={data.id} className="edit-items">
                                    <EditFileds data={data} editName={editName} editAge={editAge} editSalary={editSalary} onEditDetails={this.onEditDetails} onEditCancel={this.onEditCancel} updateEmployees={this.updateEmployees} />
                                </div>

                                : null
                            }
                            <EmployeesListItem data={data} deleteEmployees={this.deleteEmployees} editHandler={this.editHandler} />
                        </div>
                    ))
                ))
                : 'No Employees'
        )
    }

    render() {

        if (this.state.loading) {
            return <Loader />
        }
        return (
            <div>
                <div className="error-msg">{this.state.errmsg ? this.state.errmsg : null }</div>
                <div className="logout-block">
                    <button className="logout-button" title="Logout" name="Logout" onClick={this.onLogout}>Logout </button>
                </div>
                <div className="employees-heading">
                    <div> Employee Name :</div>
                    <div> Employee Age :</div>
                    <div> Employee Salary :</div>
                    <div></div>
                    <div></div>
                </div>
                <div className="list-block">
                    {this.renderEmployees()}
                </div>
            </div>
        )
    }
}


export default Home