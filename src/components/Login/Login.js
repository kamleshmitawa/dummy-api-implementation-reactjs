import React, { Component } from 'react';


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: 'admin',
            password: '123456'
        }
    }

    onLoginSubmit = (e) => {
        const { username, password } = this.state
        if (username && password) {
            localStorage.setItem('isLogin', true)
            this.props.history.push('/home')
        }
        e.preventDefault();
    }

    onChangeHandler = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value })
    }

    render() {
        const { username, password } = this.state

        return (
            <form onSubmit={this.onLoginSubmit} className="login-form" >
                <input name="username" className="username-input" placeholder="Username" value={username} onChange={this.onChangeHandler} />
                <input name="password" className="password-input" placeholder="Password" value={password} onChange={this.onChangeHandler} />
                <button type="submit" className="login-button" title="Login">Login</button>
            </form>
        )
    }
}


export default Login