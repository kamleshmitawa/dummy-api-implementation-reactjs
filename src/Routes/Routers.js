import React from 'react';
import { BrowserRouter as Router , Switch , Route } from 'react-router-dom';
import Home from '../components/Home/Home';
import NotFound from '../NotFound/NotFound';
import Login from '../components/Login/Login';
import PrivateRoute from './PrivateRoute';


const Routers = () => {
    return (
        <Router>
            <Switch>
                <PrivateRoute exact path="/" component={Login} />
                <PrivateRoute path="/home" component={Home} />
                <Route path="/login" component={Login} />
                <Route component={NotFound} />
            </Switch>
        </Router>
    )
}

export default Routers