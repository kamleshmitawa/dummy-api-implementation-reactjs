import React from 'react';
import { Route , Redirect } from 'react-router-dom';

const PrivateRoute = (props) => {
    let loginValue = localStorage.getItem("isLogin")
    return (
        loginValue ? <Route {...props} /> : <Redirect to="/login" />
    )
}

export default PrivateRoute