import axios from 'axios';

const baseURL= 'http://dummy.restapiexample.com/api/v1'

export const apiRequest = (url = '', method = '', data = {}) => {
    return axios({
        baseURL,
        method,
        url,
        data
    });
}